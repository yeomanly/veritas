# Copyright (c) 2014, Varian Medical Systems, Inc. (VMS)
# All rights reserved.
# 
# Veritas is an open source tool for TrueBeam Developer Mode provided by Varian Medical Systems, Palo Alto. 
# It lets users generate XML beams without assuming any prior knowledge of the underlying XML-schema rules. 
# This version is based on the schema for TrueBeam 1.5 and 1.6. 
#
# Veritas is licensed under the Varian Open Source License.
# You may obtain a copy of the License at:
#
#      http://radiotherapyresearchtools.com/license/
# 
# For questions, please send us an email at: TrueBeamDeveloper@varian.com                   
# 
# Developer Mode is intended for non-clinical use only and is NOT cleared for use on humans.
# Created on: 11:11:51 AM, Jul 7, 2014
# Author: Pankaj Mishra

from PySide.QtGui import QDialog, QFileDialog
import os
from UI import ui_beamON_CP, ui_beamonHeader, ui_velTable
                  
class tvTable(QDialog, ui_velTable.Ui_velTable): 
    '''
        Class for generating tolerance and velocity limit tables
    '''   
    def __init__(self, parent=None):
        super(tvTable, self).__init__(parent)
        self.setupUi(self)
        
        self.tableVal = dict()
        
        self.fields = ("GantryRtn", "CollRtn", "CouchVrt", "CouchLat", "CouchLng", 
                          "CouchRtn", "X1", "X2", "Y1", "Y2")
        self.setModal(True)  # Make it modal
 
    def accept(self):

        for field in self.fields:            
            self.tableVal['"' + field + '"'] = getattr(self, field).text() 
                        
        QDialog.accept(self)        
     
class beamonHeader(QDialog, ui_beamonHeader.Ui_beamON):
    '''
    This class launches a window prompting user to enter header 
    info, that are part of first control point in the XMl beam.
    The header info can entered in two ways: a) From scratch for a 
    new XML beam or b) Parsed from a pre-existing XMl beam created either 
    previously or generated from a DICOM-RT file.
    
    Class constructor creates an instance control point window as well as variables 
    for saving velocity and tolerance tables.  
    '''
    def __init__(self, sbeam = None, parent=None):
        
        super(beamonHeader, self).__init__(parent)
        self.setupUi(self)
        
        # Stop users from proceeding from accessing previous other windows
        self.setModal(True)                 
       
        # Variables containing tolerance table values
        self.tolTableProperties = dict()        # Mechanical axis tolerance
        self.velTableProperties = dict()        # Velocity table tolerance
        
        # Connect tolerance table push buttons  to the respective table buttons        
        self.TolTable.clicked.connect(self.openTolTable)
        self.VelTable.clicked.connect(self.openVelTable)      
                       
        # Initialize instances of tolerance table and velocity limit table window       
        self.tolTable = tvTable()
        self.velTable = tvTable()
        
        # Instantiate a control point window
        self.cpWindow = beamonCP()           
        
        # Spawn the header window
        if sbeam is not None:
            self.showHeader(sbeam)
            self.populateCP(sbeam)
        else:      
            self.showHeader()            
                      
    def populateCP(self, xmlSbeam):
        '''
        Populates the 'contorolPoint' data structure by parsing pre-existing 
        XML beam 
        :param xmlSbeam: Pre-exiting XMl beam control points
        '''
        
        tags = ("GantryRtn", "CollRtn", "CouchVrt", "CouchLat", "CouchLng", 
                         "CouchRtn", "X1", "X2", "Y1", "Y2", "Mu")
        
        cp = xmlSbeam.findall(".//Cp")        
        currCP = dict()
        cpCnt = 0
               
        for cpoint in cp:
            for child in cpoint.getchildren():
                if child.tag in tags:
                    # Parse the data for all axes in tags variable
                    currCP[child.tag] = child.text
                elif child.tag == 'Mlc':
                    # In case Mlc tag exists, extract B and A leaves and 
                    # save it in the format similar to beamON case
                    currCP['AB'] = child.find("B").text + 'AND' + child.find("A").text
                                         
            self.cpWindow.controlPoints.append(currCP)
            cpCnt += 1
            currCP = dict()
                     
    def showHeader(self, sbeam=None):
        '''
        Show the header window. Once the header info is entered (new XML beam) or 
        parsed (from a pre-existing XML beam) a list of  
        There are two possible scenarios:
        a) New XML beam: In that case header
        :param sbeam: pre-existing XML beam
        '''
        
        # headerTags = ('MLCModel', 'TolTable', 'VelTable')  
        tags = ("GantryRtn", "CollRtn", "CouchVrt", "CouchLat", "CouchLng", 
                         "CouchRtn", "X1", "X2", "Y1", "Y2", "Mu")
            
        if sbeam is not None:
        # Handle the pre-existing XML beam parses the data from the existing XML beam and 
        # populate header as well as control point window with the corresponding values    
            self.cpHeader = dict()
            # This part is for header section 
            for child in sbeam.getchildren():
                if child.tag == 'MLCModel':              
                    obj = getattr(self, child.tag)   
                    obj.setCurrentIndex(obj.findText(child.text))                    
                    self.cpHeader["MLCModel"] = child.text 
                elif child.tag == ('TolTable' or 'VelTable'):
                    pass         
                             
            cp = sbeam.findall(".//Cp")     # Extract all the control points            
            kvMV = {'k': 'kV', 'x': 'MV'}   # This part is from the first control point
     
            # Part of the header info is in first control point        
            for child in cp[0].getchildren():
                if child.tag == 'DRate':
                    getattr(self, child.tag).setText(child.text)                    
                    self.cpHeader['DRate'] =  child.text              
                elif child.tag == 'Energy':
                    getattr(self, 'EnergyValue').setText(child.text[:-1])
                    obj = getattr(self, 'EnergyType')
                    obj.setCurrentIndex(obj.findText(kvMV[child.text[-1]]))                                
                    self.cpHeader['Energy']= child.text[:-1] + child.text[-1]
                    
            # Once the header window is populated and closed, 
            # save the header info & open the control point window
            if(QDialog.exec_(self)):
            # If 'Ok' option is selected save the data and 
            # open control window 
                if cp is not None:
                    # The window self.cpWindow is already initiated in the
                    # beamonHeader class under the if else rule
                    self.cpWindow.show()                 # Now show the window
                    self.cpWindow.setWindowTitle("Control Point ")
                    self.cpWindow.dispCount.setText(str('0'))
                       
                    # For cpoint in cp:
                    for child in cp[0].getchildren():
                        if child.tag in tags:
                            obj = getattr(self.cpWindow, child.tag)
                            obj.setText(child.text)
            else:
            # In case 'cancel' or 'close' is pressed, delete the cpHeader value so that it is used 
            # but getCPHeader not to open the control pint window
                self.cpHeader = None  
                QDialog.close(self)  
        else:
        # Create a XML beam from scratch and guide the user through
        # header and control point windows     
            if(QDialog.exec_(self)):
                # If 'Ok' option is selected save the data and 
                # open control window 
                self.cpHeader = dict()
                self.getHeader()                     # Collect all the values
                self.cpWindow = beamonCP()           # Control point window
                    
                self.cpWindow.setWindowTitle("Control Point ")
                self.cpWindow.dispCount.setText(str(self.cpWindow.navCounter))
            else:
                # In case 'cancel' or 'close' pressed
                QDialog.close(self)
                 
    def openTolTable(self):
        '''
        Show tolerance table window    
        '''
        self.tolTable.show()   
        
        if(self.tolTable.exec_()):
            self.tolTableProperties = self.tolTable.tableVal

    def openVelTable(self):
        '''
        Show velocity limit table window  
        '''        
        self.velTable.show()   
         
        if(self.velTable.exec_()):
            self.velTableProperties = self.velTable.tableVal
            
    def getHeader(self):
        '''
        Save the header info from the header window in 'cpHeader'
        '''         
        self.cpHeader["MLCModel"] = self.MLCModel.currentText()        
        
        # Read in the energy value        
        if(self.EnergyType.currentText() == 'kV'):
            self.cpHeader["Energy"] =  self.EnergyValue.text() + 'k'
        elif (self.EnergyType.currentText() == 'MV'):
            self.cpHeader["Energy"] =  self.EnergyValue.text() + 'x'
        
        self.cpHeader["DRate"] =  self.DRate.text()        
         
        self.cpHeader["TolTable"] = self.tolTableProperties        
        self.cpHeader["VelTable"] = self.velTableProperties

    def getHeaderCP(self):
        '''
        Return header info for the zeroth control point.
        '''
        if hasattr(self, 'cpHeader'):
            # If the 'cpHeader' attribute has been created return its value
            return self.cpHeader        
        else:
            # In case no 'cpHeader' attribute been create just return the control back 
            return
                              
class beamonCP(QDialog, ui_beamON_CP.Ui_beamON_CP):
    '''
    Create a control point window. control point window lets the user the enter
    a set of value corresponding to the different mechanical axes. 
    In case of pre-existing XML beam this class lets the user go through 
    '''

    def __init__(self, xmlCP = None, parent = None):
        
        super(beamonCP, self).__init__(parent)
        self.setupUi(self)
        
        # Control point variable
        self.cp = dict()                                      # The overall control point
        self.controlPoints = list()                           # Now these point vary                                                                   
        
        self.fields = ("GantryRtn", "CollRtn", "CouchVrt", "CouchLat", "CouchLng", 
                          "CouchRtn", "X1", "X2", "Y1", "Y2", "Mu")
        
        self.lineedits = (self.GantryRtn, self.CollRtn, self.CouchVrt, self.CouchLat, self.CouchLng, 
                          self.CouchRtn, self.X1, self.X2, self.Y1, self.Y2, self.Mu)
    
        # Load Mlc from a text file
        self.Mlc.clicked.connect(self.openMlcFile)
        
        # For navigation and stuff
        self.navCounter = 0                       # For previous and next buttons
        self.cpCounter = 0                        # Navigation counter        
        self.nextNavFlag = False                  # To take care of navigation for second pass  
        self.prevNavFlag = False                  # To take care of navigation for first previous pass
                
        self.xmlFlag = False                      # Flag for reading XML file and propagating the previous values                                                                            
        self.newXML = False        

        #=======================================================================
        # Navigation buttons
        #=======================================================================
        self.nextButton.clicked.connect(self.nextCP)                                               # The next control point            
        self.previousButton.clicked.connect(self.prevCP)                                           # The previous point
        self.firstButton.clicked.connect(lambda: self.firstLastCP(0))                              # Jump to first control point
        self.lastButton.clicked.connect(lambda: self.firstLastCP(len(self.controlPoints)-1))       # jump to last control point
        self.randomCP.returnPressed.connect(self.randomCpoint)            

        #=======================================================================
        # Add or edit the new control point       
        #=======================================================================
        self.editAddButton.clicked.connect(self.editAddCP)            
        
        #=======================================================================
        # The final XML generation button        
        #=======================================================================
        self.doneButton.clicked.connect(self.doneCP)  # Print and close the cp dialog box  
        
        if(xmlCP):
            
            self.controlPoints = xmlCP
            self.cp = xmlCP[0]
            self.navCounter = 0
            self.displayCP(self.cp)
            
            self.xmlFlag = True
                       
    def copyCP(self):
        '''
        Copy field values from the current screen
        '''
        for (field, lineEdit) in zip(self.fields, self.lineedits):
            self.cp[field] = lineEdit.text()
                        
    def clearCP(self):
        '''
        Clear all the field values
        '''        
        for (field, lineEdit) in zip(self.fields, self.lineedits):
            self.cp[field] = lineEdit.clear()        
                        
        self.cp["AB"] = ''
        
    def displayCP(self, currentCP):        
        '''
        Display all the current points to the
        control point window
        Note: MLC leaves are not displayed
        :param currentCP:
        '''
        if("AB" in currentCP.keys()):
            del currentCP["AB"]
            
        # Propagate values in case of reading a new XML file        
        if self.xmlFlag is True:
            self.clearCP()         # Clear all the field values
           
        # Clear all the field values
        for field in currentCP.keys():
            lineEdit = self.lineedits[self.fields.index(field)]
            if currentCP[field]:
                lineEdit.setText(currentCP[field])
                                
    def openMlcFile(self):
        '''
        Add the MLC file from a text file. 
        This function assumes that MLCs are stored in a
        text file with two lines corresponding to each 
        MLC leaves.
        '''
        
        direc = "."        
        fileObj = QFileDialog.getOpenFileName(self, "Choose an MLC txt file", dir=direc, filter="Text files (*.txt)")
              
        fileName = fileObj[0]
        
        if os.path.isfile(fileName):
            fid = open(fileName, "r")            
            self.cp["AB"] = fid.readline().strip() + 'AND' + fid.readline().strip() 
            fid.close()
        else:
            print "No file selected"
                                
    def doneCP(self):
        '''
        All control points have been generated. 
        Generate XMl and show the final XML beam in the main window browser
        '''       
        # Note: After add/edit there is always an extra 
        # empty point added. Hence always remove last point
        if self.controlPoints[len(self.controlPoints)-1] is None:      
            self.controlPoints.pop()  
 
        # Close the dialog box emitting int signal '1' for exec_      
        QDialog.done(self, 1)  
 
    def firstLastCP(self, index):
        '''
        Jump to the first or last control point of the XML 
        beam. This is a common function for both first and last points.  
        :param index:
        '''
        self.dispCount.setText(str(self.navCounter))
        self.navCounter = index       
        self.displayCP(self.controlPoints[self.navCounter])        
        
        self.dispCount.setText(str(self.navCounter))
        
        # Reset navigation counter after one pass
        if(index == len(self.controlPoints))-1:
            self.cpCounter = len(self.controlPoints)-1
            
    def randomCpoint(self):
        '''
        Jump to any control point and populate the
        control point window accordingly.
        '''        
        index =  int(self.randomCP.text())
         
        if(index < len(self.controlPoints) and index >= 0):
             
            self.dispCount.setText(str(self.navCounter))        
            self.displayCP(self.controlPoints[index])        
             
            self.navCounter = index        
        
            self.dispCount.setText(str(self.navCounter))
        
    def editAddCP(self):
        '''
        Commit changes made to the control point and add it to
        the list of control points.
        '''        
        self.copyCP()

        # Create the first control point for XML from scratch 
        if(any(self.cp.values()) and self.navCounter == 0 and len(self.controlPoints) == 0):
            self.controlPoints.append(None)
            self.controlPoints[self.cpCounter] = self.cp.copy()
            
            self.cpCounter += 1 

            self.dispCount.setText(str(self.cpCounter))                        
            self.clearCP()
            
            self.newXML = True

        # Add subsequent points and also keep track of the navigation
        elif(any(self.cp.values()) and self.navCounter == len(self.controlPoints)-1 and len(self.controlPoints) > 0):

            # This is the add part
                
            self.controlPoints.append(None)
            self.controlPoints[self.cpCounter] = self.cp.copy()
                        
            #For XMl from scratch. To handle the case when the last element  
            # gets copied after the navigation has reached at the  
            if self.nextNavFlag: 
                self.cpCounter -= 1
                self.controlPoints.pop(self.cpCounter)
                self.nextNavFlag = False            # Reset the navigation flag

            # Increment for XML from scratch
            if self.newXML is True:
                self.navCounter = self.cpCounter 
    
            self.cpCounter += 1 
           
            # Increment for existing XML file
            if self.newXML is False:
                self.navCounter = self.cpCounter 
                         
            self.dispCount.setText(str(self.cpCounter))                        
            self.clearCP()
            
        # For modifying the values of exiting control points.
        # This corresponds to the edit part of the Add/Edit button
        elif(self.navCounter < len(self.controlPoints)-1 and len(self.controlPoints) > 0):
            # Edit: Change the random point 
            self.controlPoints[self.navCounter] = self.cp.copy()
        
    def compareDictElem(self, cpElem, dictElem):
        '''
        Check weather the two dictionaries are equal element-wise
        :param cpElem:
        :param dictElem:
        '''  
        k1 = cpElem.keys()
#         k2 = dictElem.keys()
        cnt = 0
        for k in k1:
            if dictElem[k] == cpElem[k]:
                cnt +=1
            else:
                cnt +=0
        if cnt == len(k1):
            return True
        else:
            return False      
                   
    def nextCP(self):       
        '''
        Go to the next control point in the control point window. 
        '''
                
        if(self.navCounter+1 < len(self.controlPoints) and self.navCounter >= 0):        # New point 

            self.navCounter += 1                               
            self.displayCP(self.controlPoints[self.navCounter])             
        
        self.dispCount.setText(str(self.navCounter))
        
        # Raise navigation flag to avoid the duplicating the last CP 
        # for XML from scratch case       
        if self.newXML is True and self.navCounter == len(self.controlPoints) -1:
            self.nextNavFlag = True
 
        # Reset navigation counter after one pass
        if(self.navCounter == len(self.controlPoints))-1 and self.newXML is False:
            self.clearCP
            self.cpCounter = len(self.controlPoints)-1
        elif(self.navCounter == len(self.controlPoints)) and self.newXML is True:
            self.cpCounter = len(self.controlPoints)-1
        self.clearCP
    
    def prevCP(self):
        '''
        Show previous control point in control point window.
        '''

        if self.controlPoints[len(self.controlPoints)-1] is None:
            self.controlPoints.pop()
                       
        if(self.newXML is False and self.navCounter >= 1):        # New point             
            self.navCounter -= 1                              
            self.displayCP(self.controlPoints[self.navCounter])                         
            self.dispCount.setText(str(self.navCounter))

        # For handling the immediate previous point in the 
        # XML file from scratch 
        elif(self.newXML is True and self.navCounter >= 1):        # New point
        
            if self.prevNavFlag is not True:
                self.displayCP(self.controlPoints[self.navCounter])                         
                self.prevNavFlag = True
                
            elif self.prevNavFlag is True:
                self.navCounter -= 1  
                self.displayCP(self.controlPoints[self.navCounter])                         

            self.dispCount.setText(str(self.navCounter))
                                                    
    def getBeamonCP(self):
        '''
        Return all control points 
        '''
        return self.controlPoints                        