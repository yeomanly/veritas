# Copyright (c) 2014, Varian Medical Systems, Inc. (VMS)
# All rights reserved.
# 
# Veritas is an open source tool for TrueBeam Developer Mode provided by Varian Medical Systems, Palo Alto. 
# It lets users generate XML beams without assuming any prior knowledge of the underlying XML-schema rules. 
# This version is based on the schema for TrueBeam 1.5 and 1.6. 
#
# Veritas is licensed under the Varian Open Source License.
# You may obtain a copy of the License at:
#
#      http://radiotherapyresearchtools.com/license/
# 
# For questions, please send us an email at: TrueBeamDeveloper@varian.com                   
# 
# Developer Mode is intended for non-clinical use only and is NOT cleared for use on humans.
# Created on: 11:11:34 AM, Jul 7, 2014
# Author: Pankaj Mishra

from PySide.QtGui import QDialog, QRadioButton, QLineEdit, QComboBox, QSpinBox, QLabel 
from xml.etree import ElementTree as ET
from UI import ui_Imaging, ui_kvImageMode, ui_mvImageMode
from utils import setBeam
import numpy as np

class kvImageMode(QDialog, ui_kvImageMode.Ui_imageMode):
    '''
    Create an kV image mode class with a set of imaging options
    kV image mode has 7 different image modes.
    '''    
    def __init__(self, parent=None):
        super(kvImageMode, self).__init__(parent)
        self.setupUi(self)
        
        self.setModal(True)
        
class mvImageMode(QDialog, ui_mvImageMode.Ui_imageMode):
    '''
    Create an MV image mode class with a set of imaging options.
    MV image mode has 4 different image modes.
    '''    
    def __init__(self, parent=None):
        super(mvImageMode, self).__init__(parent)
        self.setupUi(self) 
        
        self.setModal(True)   
    
class cpImage(QDialog, ui_Imaging.Ui_Imaging):
    '''
    Main imaging control point window. This window contains 
    kV, MV imaging points. The window also contains navigations option to 
    go backward and forward  
    '''

    def __init__(self, parent=None):
        super(cpImage, self).__init__(parent)
        self.setupUi(self)
                
        self.imagingCount = 0;
        # Open the Imaging mode option
        [r.clicked.connect(self.selectMode) for r in self.findChildren(QRadioButton)]
               
        # Gather all values: imaging cp variable and objects        
        self.currCP = dict()  # The overall control point
                    
        # Imaging Mode Options
        self.kvButtonImageMode.clicked.connect(self.openImageMode)        # Select single vs. continuous
        self.mvButtonImageMode.clicked.connect(self.openImageMode)        # Select single vs. continuous
        
        self.setModal(True)
        
    def getVariables(self, frame):
        '''
        Get variables entered in sub-windows
        :param frame: choose kV or MV sub-window
        '''        
        widgetLst = [QLineEdit, QComboBox, QSpinBox]                               # List of object to be extracted
        self.lineedits = frame.findChildren(QLabel, "kvImageModeLabel")            # For either kV or MV   
        self.lineedits.extend(frame.findChildren(QLabel, "mvImageModeLabel"))      # For either kV or MV   
                                 
        [self.lineedits.extend(frame.findChildren(lst)) for lst in widgetLst]  
            
        # Remove lineedit from spinbox    
        self.lineedits = [lst for lst in self.lineedits if lst.objectName() != 'qt_spinbox_lineedit']
        self.fields = [lst.objectName() for lst in self.lineedits] 
        
        return zip(self.fields, self.lineedits)  
        
    def selectMode(self):
        '''
        Choose kV, MV or both sub-windows
        '''        
        if(self.selectKV.isChecked()):        
            self.kvFrame.setEnabled(True)
            self.mvFrame.setEnabled(False)    
        
        elif(self.selectMV.isChecked()):                        
            self.mvFrame.setEnabled(True)
            self.kvFrame.setEnabled(False)    
        
        elif(self.selectBoth.isChecked()):        
            self.kvFrame.setEnabled(True) 
            self.mvFrame.setEnabled(True) 
    
    def openImageMode(self):
        '''
        Driver function for calling window corresponding to the 
        selected image mode. KV image mode has 7 different options while
        MV image mode has 4 different option.
        '''
        
        if self.kvFrame.isEnabled():
            mode = kvImageMode()     
            label = self.kvImageModeLabel
        elif self.mvFrame.isEnabled():
            mode = mvImageMode()      
            label = self.mvImageModeLabel  
   
        # Display the image mode dialog box
        mode.show()
        
        # Now update the label to reflect chosen value
        if(mode.exec_()):    
            [label.setText(r.objectName()) for r in mode.findChildren(QRadioButton) if r.isChecked()]          
                                         
    # Navigation options    
    def prevCP(self):
        '''
        Go to previous imaging control point
        '''
        pass
    
    def nextCP(self, fieldVars):
        '''
        Save all the values for current control point and 
        jump to the next imaging control point.
        :param fieldVars:
        '''        
        # Save all the field values
        for (field, lineEdit) in fieldVars:    
            if isinstance(lineEdit, QLineEdit):
                self.currCP[field] = lineEdit.text()                                
            elif isinstance(lineEdit, QComboBox):
                self.currCP[field] = lineEdit.currentText()                
            elif isinstance(lineEdit, QSpinBox):
                self.currCP[field] = str(lineEdit.value())        
            elif isinstance(lineEdit, QLabel):
                self.currCP[field] = str(lineEdit.text())
                
    def clearCP(self, fieldVars):
        '''
        Clear all the field variables in the current
        sub-window
        :param fieldVars:
        '''        
        # Default kv imaging parameters value
        sbValue = {"KiloVolts": 40, "MilliAmperes": 20, "MilliSeconds": 10}
        
        # Clear all the field values
        for (field, lineEdit) in fieldVars:
            if isinstance(lineEdit, QLineEdit):
                lineEdit.clear()
            
            elif isinstance(lineEdit, QSpinBox):                
                lineEdit.setProperty("value", sbValue[lineEdit.objectName()])

        # Switch to the default option
        if self.kvFrame.isEnabled():
            self.kvImageModeLabel.setText('DynamicGain')    # Default imaging mode                 
            self.kvAxis.setEnabled(False)                   # Now the axis is locked
        elif self.mvFrame.isEnabled():   
            self.mvImageModeLabel.setText('Continuous')     # Default imaging mode                
            self.mvAxis.setEnabled(False)                   # Now the axis is locked
                 
class imageWindow():
    '''
    Open the imaging window. 
    Imaging window can currently handle kV, 
    MV as well as both kV and MV control points.     
    '''
    
    def __init__(self):
                        
        # dictionary(kv, mv) of list (number of cp) of dictionary(field values)               
        self.imagingPoints = {"kv" : [], "mv" : []}         # Dict of list imaging CP (both kv & mv)           
        self.cpImage = cpImage()                            # Instantiate the header window
                
    def openImaging(self):
        '''
        Driver function for opening the imaging window
        '''
                               
        self.cpImage.nextButton.clicked.connect(self.copyCP)        # Save the current cp and get next cp
        self.cpImage.show()                                         # Show the header window  
            
    def copyCP(self):
        '''
        Copy the values for fractional imaging control points 
        from imaging sub-windows
        '''
        
        kvf = self.cpImage.kvFrame
        mvf = self.cpImage.mvFrame
        
        # Add header                
        with open('output\\output.xml', 'r') as f:
            tree = ET.parse(f)
            root = tree.getroot()            
            sbeam = root.getchildren()[0]
            
        if(kvf.isEnabled()):
            fieldVars = self.cpImage.getVariables(kvf)            
            self.cpImage.nextCP(fieldVars)                                    # Extract the current control point#           
            self.imagingPoints["kv"].append(self.cpImage.currCP.copy())       # Save previous cp
            self.cpImage.clearCP(fieldVars)                                   # Clear all the fields in cp
          
            # imaging count
            if(self.cpImage.imagingCount == 0):                    
                iparams = ET.SubElement(sbeam, 'ImagingParameters')
                
                if self.cpImage.kvOutsideTreatment.isChecked():
                    outside = ET.SubElement(iparams, 'OutsideTreatment')
                    ET.SubElement(outside, 'MaxMu').text = '0'
                else:
                    ET.SubElement(iparams, 'DuringTreatment')
                  
#                 ET.SubElement(iparams, 'DuringTreatment')
                ET.SubElement(iparams, 'ImagingPoints')
                pt = float(self.cpImage.currCP["kvAxisValue"])  
            elif(self.cpImage.imagingCount > 0):
                pt = float(self.cpImage.currCP["kvAxisValue"])  
                                        
            # increment count
            self.cpImage.imagingCount += 1
                 
            #===================================================================
            # Get all the axis values. First read the selected axis name from 
            # self.cpImage.currCP["kvAxis"]) and then tree.iter extracts all the
            # values in "values" 
            #===================================================================
            
            values = list() 
            [values.append(float(v.text)) for v in tree.iter(self.cpImage.currCP["kvAxis"])]
            
            # Take care of counter clockwise Gantry Rotation
            valuesBefore = values
            if self.cpImage.currCP["kvAxis"] == "GantryRtn":
                values = np.array((np.sort(np.asarray(values)))).tolist()                               

            # Add the imaging point
            cp = self.calculateFraction(values, pt)
            
            # If reverse rotation flip the calculated cp
            if not np.array_equal(np.asarray(valuesBefore), np.asarray(values)):
                cp = 1-cp
            
            # Insert the fractional imaging control point    
            self.kvImagingNode(root, cp) 
             
        elif(mvf.isEnabled()):
            fieldVars = self.cpImage.getVariables(mvf)
            self.cpImage.nextCP(fieldVars)                                    # Extract the current control point
            self.imagingPoints["mv"].append(self.cpImage.currCP.copy())       # Save previous cp
            self.cpImage.clearCP(fieldVars)                                   # Clear all the fields in cp
         
            # imaging count
            if(self.cpImage.imagingCount == 0):
                iparams = ET.SubElement(sbeam, 'ImagingParameters')
                ET.SubElement(iparams, 'DuringTreatment')                    
                ET.SubElement(iparams, 'ImagingPoints')
                pt = float(self.cpImage.currCP["mvAxisValue"])  
            elif(self.cpImage.imagingCount > 0):
                pt = float(self.cpImage.currCP["mvAxisValue"])    
            
            # Get all the axis values
            values = list() 
            [values.append(float(v.text)) for v in tree.iter(self.cpImage.currCP["mvAxis"])]
                         
            # increment count
            self.cpImage.imagingCount += 1

            # Add the imaging point
            cp = self.calculateFraction(values, pt)
            self.mvImagingNode(root, cp)
    
        # Validate and write output XML file
        setBeam.parse(root, 'output\\output.xml', True)
              
    def calculateFraction(self, values, pt):
        '''
        Calculate fractional control point based on 
        an array of control point 'values' and the imaging point 'pt'
        :param values: An array containing al the control points
        :param pt: Value of fractional control point
        '''
        # Get fractional control point   
        if pt == values[0]:
            cp = 0
        elif pt == values[-1]:
            cp = len(values) -1
        else:                 
            a = next(x[0] for x in enumerate(values) if x[1] > pt)
            cp = ((a-1)+(values[a-1]-pt)/(values[a-1]-values[a]))
        
        # Round the control point to 4 decimal places
        cp = np.around(cp, decimals=2)     
        
        return cp
          
    def kvImagingNode(self, root, cp):
        '''
        Create an XML object corresponding to the kV imaging points.
        These XMl tags are based on XML schema 1.5
        :param root: The root element of the existing XML beam.
        :param cp: MV imaging control point to be added to the 'root'
        '''
        
        node = root.findall(".//ImagingPoints")
                
        elem = ET.SubElement(node[0], "ImagingPoint")       # Add Imaging point tag        
        ET.SubElement(elem,"Cp").text = str(cp)             # Add fractional control point (cp)
        
        # Single vs. Continuous imaging mode
        continuousImageModes = ['DynamicGainFluoro', 'DynamicGainLowFramerateFluoro', 'Fluoro']
        singleImageModes = ['DynamicGain', 'DynamicGainLowFramerate', 'LowDose', 'HighQuality']

        imageModeStart = root.findall(".//AcquisitionStart") 
        imageModeStop = root.findall(".//AcquisitionStop") 
                
        if not imageModeStart or (len(imageModeStart) == len(imageModeStop)): 

            if self.cpImage.currCP["kvImageModeLabel"] in singleImageModes:
                ac = ET.SubElement(elem,"Acquisition")              
            elif self.cpImage.currCP["kvImageModeLabel"] in continuousImageModes:
                ac = ET.SubElement(elem,"AcquisitionStart")
                    
            ET.SubElement(ac,"AcquisitionId").text = str(self.cpImage.imagingCount)
            
            acSpecs = ET.SubElement(ac,"AcquisitionSpecs")
            ET.SubElement(acSpecs,"Handshake").text = "true"
            ET.SubElement(acSpecs,"KV").text = "true"
            
            params = ET.SubElement(ac,"AcquisitionParameters")
           
            ET.SubElement(params,"ImageMode", id = self.cpImage.currCP["kvImageModeLabel"])
            ET.SubElement(params,"CalibrationSet").text = "DefaultCalibrationSetId"
            
            kvWattage = ET.SubElement(params,"KV")
            ET.SubElement(kvWattage,"KiloVolts").text =  str(self.cpImage.currCP["KiloVolts"])
            ET.SubElement(kvWattage,"MilliAmperes").text =  str(self.cpImage.currCP["MilliAmperes"])
            ET.SubElement(kvWattage,"MilliSeconds").text =  str(self.cpImage.currCP["MilliSeconds"])
            ET.SubElement(kvWattage,"eFluoroLevelControl").text = "None"
            
            # Optional kV detector and source tags
            # kV detector Kvd tag (OPtional)
            kvd = ET.SubElement(elem, 'Kvd')
            pos = ET.SubElement(kvd, 'Positions')
            
            if self.cpImage.currCP["kvdLat"]:
                ET.SubElement(pos, 'Lat').text = str(self.cpImage.currCP["kvdLat"])
            if self.cpImage.currCP["kvdLng"]:
                ET.SubElement(pos, 'Lng').text = str(self.cpImage.currCP["kvdLng"])
            if self.cpImage.currCP["kvdVrt"]:
                ET.SubElement(pos, 'Vrt').text = str(self.cpImage.currCP["kvdVrt"])
            if self.cpImage.currCP["kvdPitch"]:
                ET.SubElement(pos, 'Pitch').text = str(self.cpImage.currCP["kvdPitch"])
             
            # kV source Kvs tag (Optional)
            kvs = ET.SubElement(elem, 'Kvs')
            pos = ET.SubElement(kvs, 'Positions')
            if self.cpImage.currCP["kvsLat"]:
                ET.SubElement(pos, 'Lat').text = str(self.cpImage.currCP["kvsLat"])
            if self.cpImage.currCP["kvsLng"]:
                ET.SubElement(pos, 'Lng').text = str(self.cpImage.currCP["kvsLng"])
            if self.cpImage.currCP["kvsVrt"]:
                ET.SubElement(pos, 'Vrt').text = str(self.cpImage.currCP["kvsVrt"])
            if self.cpImage.currCP["kvsPitch"]:
                ET.SubElement(pos, 'Pitch').text = str(self.cpImage.currCP["kvsPitch"])
                        
            # kV Filters (Optional)
            kvFilters = ET.SubElement(elem,'KvFilters')
            if self.cpImage.currCP["kvFoil"]:
                ET.SubElement(kvFilters, 'Foil').text = str(self.cpImage.currCP["kvFoil"])
            if self.cpImage.currCP["kvShape"]:
                ET.SubElement(kvFilters, 'Shape').text = str(self.cpImage.currCP["kvShape"])
                                                                     
        elif(len(imageModeStop) < len(imageModeStart)): 
            ac = ET.SubElement(elem,"AcquisitionStop")
            ET.SubElement(ac,"AcquisitionId").text = str(self.cpImage.imagingCount-1)
            ET.SubElement(ac,"AcquisitionSpecs")        
                    
    def mvImagingNode(self, root, cp):
        '''
        Create XML object corresponding to the MV imaging points.
        This XMl tags are based on XML schema 1.5
        :param root: The root element of the existing XML beam.
        :param cp: MV imaging control point to be added to the 'root'
        '''        
        node = root.findall(".//ImagingPoints")
        elem = ET.SubElement(node[0],"ImagingPoint")      # Add Imaging point tag
        ET.SubElement(elem,"Cp").text = str(cp)           # Add fractional control point (cp)
        
        # Define single and continuous imaging mode types 
        continuousImageModes = ['Continuous', 'Dosimetry']
        singleImageModes = ['Highres', 'Lowres']

        # Look for pre-existing continuous mode XML tags
        
        # Add the appropriate i.e., single or continuous XML tag
        
        # Check if there are any continuous tags  
                                           
        ac = ET.SubElement(elem,"Acquisition")            # Acquisition Tag
        ET.SubElement(ac,"AcquisitionId").text = str(self.cpImage.imagingCount)
        ET.SubElement(ac,"AcquisitionSpecs")
        
        params = ET.SubElement(ac,"AcquisitionParameters")
        ET.SubElement(params,"ImageMode", id = self.cpImage.currCP["mvImageModeLabel"])
        ET.SubElement(params,"CalibrationSet").text = "DefaultCalibrationSetId"
        ET.SubElement(params,"MV")        
            
        # Finish adding the continuous tag     