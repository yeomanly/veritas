from PySide.QtGui import QVBoxLayout, QMessageBox
from xml.etree import ElementTree as ET
import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'  
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np

# Flag for switching between MU vs Gantry or all the axes
allAxes = False

def viewAxes():
    '''
    Plots Gantry Rotation (GantryRtn) as a function as a function of Monitor Units (MUs)
    Also adds vertical lines where the kV or MV imaging points are scheduled. 
    '''
    # Generate the plot
    q = QVBoxLayout()  
    fig = Figure(figsize=(5.0,4.0), dpi=72, facecolor=(1,1,1), edgecolor=(0,0,0))
    ax = fig.add_subplot(111)        
    canvas = FigureCanvas(fig)
    q.addWidget(canvas)
       
    # Now plot
    with open('output\\output.xml', 'r') as f:
        tree = ET.parse(f)
        root = tree.getroot()
               
    gantry = list()
    dose = list()
     
    gntrLst =  root.findall(".//ControlPoints/Cp/GantryRtn")
    muLst =  root.findall(".//ControlPoints/Cp/Mu")
    
    if not allAxes:
        if (not gntrLst) or (not muLst):
            msgBox = QMessageBox()
            msgBox.setWindowTitle('Veritas plotting error')
            msgBox.setText("Currently only Gantry rotation vs. MU supported")
            msgBox.exec_()      
            return
        
        for gaMU in root.findall(".//ControlPoints/Cp"):
    
            ga = gaMU.find('GantryRtn')
            mu = gaMU.find('Mu')  
                                 
            if (ga is not None) and (mu is not None):
                # Record both gantry and MU values
                gantry.append(float(ga.text))
                dose.append(float(mu.text))
            elif ga is None:
                # When gantry is not moving record the previous value
                gantry.append(gantry[-1])
                dose.append(float(mu.text))
            
        # Extract imaging dose values and imaging types
        ip = list()
        imagingType = list()
    
        for d in root.findall(".//ImagingPoint"):
            ip.append(float(d.find('Cp').text))
    
            if d.find('.//KV') is not None:
                imagingType.append("kV")
            elif d.find('.//MV') is not None:
                imagingType.append("MV")
    
        # Plot imaging points
        plotColor = {"kV": "blue", "MV": "red"}
        labelInd = 0; # For stopping legend repetition
        if ip is not None:
            for value, flag in zip(ip, imagingType):
                idx, frac = int(value), value-int(value)
                pt = dose[int(idx)] + frac*(abs(dose[idx] - dose[idx+1]))
                ax.plot(pt*np.ones(len(gantry)), dose, color = plotColor[flag], \
                         label = flag if labelInd < 2 else "") # Trick to stop repetition of legend label
                labelInd += 1             
           
        # Plot control points
        ax.plot(dose, gantry, color = "green", label= 'Gantry \n rotation')
           
        # Now the labeling part
        ax.set_xlabel('Monitor Units (MUs)')
        ax.set_ylabel('Gantry Rotation')         
        ax.set_title('Gantry Vs. MU Plots')
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, labels)        
        canvas.show()
        
    else:
        print "All axes flag on"   

        