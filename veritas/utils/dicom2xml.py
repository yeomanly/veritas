import dicom
from xml.etree import ElementTree as ET
import numpy as np
from utils import setBeam


class Dicom2Xml:
    
    def __init__(self, filename):
        self.filename = filename
        
    def dicomToDataset(self):
        dataset = dicom.read_file(self.filename, force=True)
        self.ds = dataset
    
    def getLeafJaws(self, leafJawsSequence, cp):
        ''' Extract Mlc, X or Y jaws (if present)
         and then add it to the existinXML string 
        '''
        selectionStr = {"MLCX": "Mlc", "ASYMX": "X", "ASYMY": "Y", "X": "X", "Y": "Y"}        
        
        for lj in leafJawsSequence:
            if(selectionStr[lj.RTBeamLimitingDeviceType] == "Y"):
                ET.SubElement(cp, 'Y1').text = str(lj.LeafJawPositions[:1])[2:-2]
                ET.SubElement(cp, 'Y2').text = str(lj.LeafJawPositions[1:])[2:-2]
            elif(selectionStr[lj.RTBeamLimitingDeviceType] == "X"):
                ET.SubElement(cp, 'X1').text = str(lj.LeafJawPositions[:1])[2:-2]
                ET.SubElement(cp, 'X2').text = str(lj.LeafJawPositions[1:])[2:-2]
            elif(selectionStr[lj.RTBeamLimitingDeviceType] == "Mlc"):
                mlcTag = ET.SubElement(cp,'Mlc')
                ET.SubElement(mlcTag,'ID').text = '1'
                ET.SubElement(mlcTag, 'B').text = str(lj.LeafJawPositions[:60]).replace("', '", " ")[2:-2]
                ET.SubElement(mlcTag, 'A').text = str(lj.LeafJawPositions[60:]).replace("', '", " ")[2:-2]       
        
    def findMlcModel(self, inMlc):
        '''
        Determine the MLCModel type. Currently NDS120, NDS120HD, NDS80 are supported.
        :param inMlc: MLC array from the DICOM file
        '''
        
        # MLC model pattern NDS120 (in mm) 
        firstLast10 = np.array(10*np.ones(10, dtype='float', order = 'C'))
        middle40 =  np.array(5*np.ones(40, dtype='float', order = 'C'))
        nds120 = np.concatenate((firstLast10, middle40, firstLast10))
 
        # MLC model pattern ND120HD (in mm)
        firstLast14 = np.array(5*np.ones(14, dtype=np.float32, order = 'C'))
        middle32 =  np.array(2.5*np.ones(32, dtype=np.float32, order = 'C'))
        nds120HD = np.concatenate((firstLast14, middle32, firstLast14))  
        
        # Now some constants
        mlcLength = len(inMlc)                         
        if mlcLength == 61:
            if np.all(np.diff(inMlc) == nds120):
                return 'NDS120'
            elif np.all(np.diff(inMlc) == nds120HD):
                return 'NDS120HD'
        elif mlcLength == 41:
            return 'NDS80'
      
    def extractControlPoints(self):
                
        lookupTable = {'NominalBeamEnergy' : 'Energy', \
                       'DoseRateSet' : 'DRate', \
                       'GantryAngle' : 'GantryRtn', \
                       'BeamLimitingDeviceAngle' : 'CollRtn',\
                       'TableTopVerticalPosition' : 'CouchVrt',\
                       'TableTopLongitudinalPosition' : 'CouchLng', \
                       'TableTopLateralPosition' : 'CouchLat',\
                       'CumulativeMetersetWeight' : 'Mu',\
                       'BeamLimitingDevicePositionSequence' : 'MlcJaws'
                       }
        
        energyType = {"PHOTON": "x", "ELECTRON": "e"}
    
        dose = 0;
        
        root = ET.Element('VarianResearchBeam')
        sb = ET.SubElement(root,'SetBeam')
        ET.SubElement(sb, 'Id').text = '1'

        # Determine the MLCModel type: NDS80, NDS120, NDS120HD
        inMlc = [float(s) for s in self.ds.BeamSequence[0].BeamLimitingDeviceSequence[2].LeafPositionBoundaries]
        ET.SubElement(sb, 'MLCModel').text = self.findMlcModel(inMlc)
        
        ET.SubElement(sb, 'Accs').text = ''
        
        controlpoint = ET.SubElement(sb, 'ControlPoints')            
        dosePrev = 0.0  # For cumulative dose
            
        for (beam, frac) in zip(self.ds.BeamSequence, self.ds.FractionGroupSequence[0].ReferencedBeamSequence):
                        
            # Control point
            for cnt in range(0, beam.NumberofControlPoints):
                cp = ET.SubElement(controlpoint, 'Cp')
            
                for elem in beam.ControlPointSequence[cnt]:                    
                    keyTag = elem.name.replace(" ", "") 
                    if(keyTag in lookupTable.keys()):
                        if (lookupTable[keyTag] == 'Energy'): 
                            ET.SubElement(cp, lookupTable[keyTag]).text = str(elem.value) + energyType[beam.RadiationType]
                        elif (lookupTable[keyTag] == "MlcJaws"):
                            self.getLeafJaws(elem, cp)
                        elif(lookupTable[keyTag] == "Mu"):
                            dose = dosePrev + frac.BeamMeterset*elem.value
                            ET.SubElement(cp, lookupTable[keyTag]).text = str(dose)
                        else:
                            ET.SubElement(cp, lookupTable[keyTag]).text = str(elem.value)
            
            dosePrev = dosePrev + frac.BeamMeterset # Cumulative dose
        
        #------------------------------------ Validate and write output XML file
        setBeam.parse(root, 'output\\output.xml', True)
