# Copyright (c) 2014, Varian Medical Systems, Inc. (VMS)
# All rights reserved.
# 
# Veritas is an open source tool for TrueBeam Developer Mode provided by Varian Medical Systems, Palo Alto. 
# It lets users generate XML beams without assuming any prior knowledge of the underlying XML-schema rules. 
# This version is based on the schema for TrueBeam 1.5 and 1.6. 
#
# Veritas is licensed under the Varian Open Source License.
# You may obtain a copy of the License at:
#
#      http://radiotherapyresearchtools.com/license/
# 
# For questions, please send us an email at: TrueBeamDeveloper@varian.com                   
# 
# Developer Mode is intended for non-clinical use only and is NOT cleared for use on humans.
# Created on: 11:11:18 AM, Jul 7, 2014
# Author: Pankaj Mishra

from PySide.QtGui import QMainWindow, QDialog, QIcon, QFileDialog, QMessageBox, QApplication
import sys, os, win32api
from xml.etree import ElementTree as ET
from  UI import ui_mainWindow, ui_licenseDlg
import imaging, beamon
from utils import dicom2xml, plotXML, setBeam

class LicenseAgreement(QDialog, ui_licenseDlg.Ui_licenseDlg):
    '''
    Show the license window before the application starts
    '''
    def __init__(self, parent=None):
        
        super(LicenseAgreement, self).__init__(parent)
        self.setupUi(self)                
        self.setWindowTitle("Veritas: License Agreement")
        self.setModal(True)
        
        self.acceptButton.clicked.connect(self.startVeritas)
        self.cancelButton.clicked.connect(self.cancelVeritas)
        
    def startVeritas(self):
        '''
        User accepted the license agreement and is allowed to proceed
        '''
        self.mainwindow = MainWindow()            
        self.hide()                     # Hide is used as close calls the close event function
        self.mainwindow.show()
    
    def cancelVeritas(self):
        '''
        user decides not to proceed
        '''
        self.close()
    
    def closeEvent(self, event):
        '''
        Double check with the user.
        Also, alerts user in case he/ she accidentally presses the 
        close button
        :param event:
        '''  
        reply = QMessageBox.question(self, "Veritas", "Are you sure you want to quit?", QMessageBox.Yes|QMessageBox.No)

        if reply == QMessageBox.Yes:
            event.accept()
        elif reply == QMessageBox.No:
            event.ignore()        

class MainWindow(QMainWindow, ui_mainWindow.Ui_MainWindow):
    '''The main window class is the entrance point for both DICOM-RT based 
    XML generation/modification as well as for the XML-from-scratch.
    '''

    def __init__(self, parent=None):
        
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)                
        self.setWindowTitle("Veritas: TrueBeam Developer Mode")
        self.setWindowIcon(QIcon('truebeam2.jpg'))  
        
        # self.gatingButton.setEnabled(False)
        self.gatingButton.hide()
        #=======================================================================
        # The beamON part         
        #=======================================================================   
        self.beamonButton.clicked.connect(self.openBeamonHeader)      # Connect to the beam on  functionality  
        self.cpHeader = dict()                                        # A set of attributes for data
        self.cpControlPoints = list()       

        #=======================================================================
        # Imaging part
        #=======================================================================        
        self.imagingButton.clicked.connect(self.openImagingWindow)       
  
        #=======================================================================
        # Final functionality
        #=======================================================================  
        self.xmlTextEdit.clicked.connect(self.openTextEditor)       # Load XML file in a text editor
        self.plotAxes.clicked.connect(plotXML.viewAxes)             # Plot the axes
          
        #=======================================================================
        # Now the Menu plan
        #=======================================================================          
        self.actionOpenPlan.triggered.connect(self.openXMLPlan)
        self.actionCreatePlan.triggered.connect(self.createPlan)
        self.actionDcm2xml.triggered.connect(self.dcm2xml)
        self.actionExit.triggered.connect(self.close)
        self.actionAbout.triggered.connect(self.aboutVeritas)
        self.actionDocuments.triggered.connect(self.viewManual)
        self.actionLicense.triggered.connect(self.viewLicense)
        
    def openBeamonHeader(self, sbeam=None): 
        '''Open the header part of XML generation. Header basically refers to 
        the header as well as part of the control point 0.
        :param sbeam
        sbeam=None: build XMLbeam file from scratch  
        sbeam=XMLbeam: process the XMLbeam either generated via DICOM2XMl conversion or 
        an already existing XMLbeam file'''
        if sbeam is not None:
            #------------------ Process the Header info for an existing XML file
            self.cpGUI =  beamon.beamonHeader(sbeam)
            self.cpHeader = self.cpGUI.getHeaderCP()
        else:
            #----------- Open the Header info window for an XMLbeam from scratch
            self.cpGUI =  beamon.beamonHeader()
            self.cpHeader = self.cpGUI.getHeaderCP()

        #=======================================================================
        # When the doneButton is pressed from the control point
        # exec_()  generates int 1 as a 'signal'. After receiving the signal 
        # the control point window 1 is closed and XML file is generated         
        #=======================================================================
        
        if self.cpHeader is not None: # To exclude close button and 'cancel' scenario
            if(self.cpGUI.cpWindow.exec_() == 1):
                self.cpControlPoints = self.cpGUI.cpWindow.getBeamonCP()
                self.exportXML()
    
    def openImagingWindow(self):        
        #------------------------------- Instantiate and open the imaging window
        self.imagingGui = imaging.imageWindow()             # Instantiate an imaging window
        self.imagingGui.openImaging()                       # Open the imaging window
        self.imagingGui.cpImage.doneButton.clicked.connect(self.imgOutput) # Done button for XMLbeam output

    def displayOutput(self,fname):        
        '''
        Read the XML file line-by-line and display it on the main window text-browser
        Note: '<' and '>' needs to be replaced by &lt and &gt to display the XML tags correctly
        This is important in case of bannkB which is otherwise mistaken as 'bold' html tag. 
        :param fname:
        '''
        # Start with a clear browser display
        # Also, cleans up the previous window display
        self.textBrowser.clear()
        
        # Read in xml file
        xmlFile = open(fname, "r")
        outputStr = xmlFile.readlines()   
        xmlFile.close()
            
        # Replace '<' with &lt and '>' with '&gt'.
        # This is done to avoid confusion. 
        # e.g., <B> mlc xml tag can be interpreted as bold
        
        for line in outputStr: 
            line = line.replace("<","&lt;")
            line = line.replace(">","&gt;")
            line = line + '\n'
            self.textBrowser.append('%s' % line)    
                           
    def imgOutput(self):
        '''
        Display the output xml image. 
        This function assumes the generated XML file is named 'output.xml'
        '''
        self.displayOutput('output\\output.xml')
        self.imagingGui.cpImage.close() 
           
    def exportXML(self):
        '''
        Converts all the control points entered by users into an XMl
        file as per the SetBeam XMl schema
        '''
          
        # Copy if there is any thing useful in the end            
        root = ET.Element('VarianResearchBeam')
        tree = ET.ElementTree(root)
        root.set('SchemaVersion', '1.0')
        beam = ET.Element('SetBeam')
        root.append(beam)
                  
        ET.SubElement(beam, 'Id').text = '1'
        
        if 'MLCModel' in self.cpHeader.keys():
            ET.SubElement(beam, 'MLCModel').text = str(self.cpHeader['MLCModel'])
   
        # Here the tolTable and velTable comes in to picture
        if "TolTable" in self.cpHeader.keys():
            tolTable = ET.SubElement(beam,'TolTable')
            for key, value in self.cpHeader["TolTable"].items():
                if value:
                    ET.SubElement(tolTable, key[1:len(key)-1]).text = str(value)

        if "VelTable" in self.cpHeader.keys():
            velTable = ET.SubElement(beam,'VelTable')
            for key, value in self.cpHeader["VelTable"].items():
                if value:
                    ET.SubElement(velTable, key[1:len(key)-1]).text = str(value) 
   
        # Now starts the Accs part    
        ET.SubElement(beam, 'Accs')
        cpts = ET.SubElement(beam, 'ControlPoints')
        cpt = ET.SubElement(cpts,'Cp') 
        sbeam = ET.SubElement(cpt, 'SubBeam')
        ET.SubElement(sbeam, 'Seq').text = '0'
        ET.SubElement(sbeam, 'Name').text = 'Beam ON!'
 
        # Now the second half of header
        ET.SubElement(cpt, 'Energy').text =  self.cpHeader["Energy"]      
        ET.SubElement(cpt, 'DRate').text = self.cpHeader["DRate"]
                        
        # For the header control point
        for key, value in self.cpControlPoints.pop(0).items():                             
            if key == "AB" and value:
                value = value.split("AND")
                mlc = ET.SubElement(cpt, 'Mlc')
                ET.SubElement(mlc, 'ID').text = '1'
                ET.SubElement(mlc,'B').text = str(value[0])
                ET.SubElement(mlc, 'A').text = str(value[1])
            elif value:
                ET.SubElement(cpt, key).text = str(value) 
                    
        # Now the automatic part        
        for controlPoints in self.cpControlPoints:   
            cp = ET.SubElement(cpts, 'Cp')    
            for key, value in controlPoints.items():                            
                if key == "AB" and value:
                    value = value.split("AND")
                    mlc = ET.SubElement(cp, 'Mlc')
                    ET.SubElement(mlc, 'ID').text = '1'
                    ET.SubElement(mlc, 'B').text = str(value[0])
                    ET.SubElement(mlc, 'A').text = str(value[1])
                elif value:
                    ET.SubElement(cp, key).text = str(value) 
                  
        # Validate and write output XML file
        setBeam.parse(root, 'output\\output.xml', True)
        # Display in browser's main window
        self.displayOutput("output\\output.xml")
         
    def openTextEditor(self):
        '''
        Open the final xml file. This function assumes the availability of 
        notepadd++ text editor.
        '''
             
        fileObj = QFileDialog.getOpenFileName(self, "Choose output XML file", dir=".", filter="Text files (*.xml)")               
        fileName = fileObj[0]
         
        if os.path.isfile(fileName):
            os.system('start notepad++ '+ fileName)
        else:
            QMessageBox.information(self, "XML file", '''Either no file selected or missing notepad++ editor''', QMessageBox.Ok)
                        
    #===========================================================================
    #  Function in Toolbar area
    #===========================================================================     
    def openXMLPlan(self):
        '''
        Parse an existing XMl plan and display the file in header and subsequent 
        control point windows. This function can be launched if an xml file already
        exists or an xml file has been generated from a DICOM file using Dcm2Xml function 
        '''
         
        fileObj = QFileDialog.getOpenFileName(self, "Choose an .xml file", dir="input", filter="Text files (*.xml)")               
        fileName = fileObj[0]
         
        if os.path.isfile(fileName):
            self.loadFile(fileName)
        else:
            QMessageBox.information(self, "XML file", '''No XML file selected''', QMessageBox.Ok)
            
    def createPlan(self):
        '''
        Starts generation of an XMl file from scratch 
        '''
        self.openBeamonHeader()
        
    def dcm2xml(self):
        '''
        Calls the DICOM-RT to XML conversion function. 
        A message box conveys the completion of XML file generation.
        '''
        
        fileObj = QFileDialog.getOpenFileName(self, "Choose a DICOM-RT file", dir="input", filter="Text files (*.DCM; *.dcm)")
               
        fileName = fileObj[0]         
        if os.path.isfile(fileName):
            x = dicom2xml.Dicom2Xml(fileName)    
            x.dicomToDataset()
            x.extractControlPoints()               
            # Message that a specific file has been generated    
            QMessageBox.information(self, "DICOM to XML conversion", '''XML file has been generated''', QMessageBox.Ok)   
        else:
            # No DICOM file has been selected
            QMessageBox.information(self, "DICOM to XML conversion", '''No DICOM file selected''', QMessageBox.Ok)
                                
    def loadFile(self, fileName):
        '''
        calls beamonHeader to load an existing XML file in
        the Control point windows
        :param fileName: 
        '''
        
        tree = ET.parse(fileName)
        root = tree.getroot()            
        sbeam = root.getchildren()[0]

        self.openBeamonHeader(sbeam)
        
    def aboutVeritas(self):
        '''
        Print one line description, research only message line 
        and contact email in a message 
        '''
        QMessageBox.about(self,"About Veritas",
                          """<b> Veritas v 1.0 </b>
                          <p>Veritas is an open source tool for TrueBeam 
                          Developer Mode provided by Varian Medical Systems, Palo Alto. 
                          <p>It lets users generate XML beams without assuming any 
                          prior knowledge of the underlying XML-schema rules
                          <p>This version is based on the schema for TrueBeam 1.5 and 1.6                          
                          <p>For questions, please send us an email at: TrueBeamDeveloper@varian.com                          
                          <p> <b> Developer Mode is intended for non-clinical use only 
                          and is NOT cleared for use on humans </b> """)
        
    def viewManual(self):
        '''
        Display the TrueBeam Developer Mode manual in acrobat reader
        '''       
        fileName = 'TrueBeamDeveloperModeManual_V_0.2.0_03_2012.pdf'
        win32api.ShellExecute(0, "open", fileName, None, "", 1)

    def viewLicense(self):
        '''
         Display the Veritas BSD license in acrobat reader
        '''
        fileName = 'Veritas_License.pdf'
        win32api.ShellExecute(0, "open", fileName, None, "", 1)
    
    def closeEvent(self, event):
        '''
        Double check with the user that he/she wants to
        quit Veritas
        :param event:
        '''      
        reply = QMessageBox.question(self, "Veritas",
                                     "Are you sure you want to quit?",
                                     QMessageBox.Yes|QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()
        elif reply == QMessageBox.No:
            event.ignore()
                                          
if __name__ == "__main__":

    app = QApplication(sys.argv)
    form = LicenseAgreement()
    form.show()
    sys.exit(app.exec_())
