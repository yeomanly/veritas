Readme
======

Veritas is an open source tool for TrueBeam Developer Mode provided by Varian Medical Systems, Palo Alto.
It lets users generate XML beams without assuming any prior knowledge of the underlying XML-schema rules.
This version is based on the schema for TrueBeam 1.5 and 1.6.

For questions, please send us an email at: TrueBeamDeveloper@varian.com


Features
========

Main features included in version 1.0 are as follows:

* **Create** a new XML beam.
* **Modify** an existing XML beam
* **Convert** DICOM-RT to an XML.
* **Insert imaging points** (kV and MV) in an XML beam.

Project Details
===============

:Code:            https://bitbucket.org/varianveritas/veritas/
:Issue tracker:   https://bitbucket.org/varianveritas/veritas/issues/
:Documentation:   https://bitbucket.org/varianveritas/veritas/doc/
:License:         Varian Open Source 1.0

.. warning::

**Developer Mode is intended for non-clinical use only and is NOT cleared for use on humans**
