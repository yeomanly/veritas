
Installation
************

Welcome to the installation page.


Requirements
============

The list of dependencies are as follows:

* Python 2.7

* PySide 1.20

* pydicom 0.9.8

* matplotlib 1.3.1

These librabries can installed either through easy_install

or pip


Supported platform
==================

* Microsoft Windows 7

* GNU/Linux - ubuntu 12.04 LTS
