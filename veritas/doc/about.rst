About Varian Veritas
====================

Varian Veritas version 1.0 is an open source tool for TrueBeam Developer Mode provided by Varian Medical Systems, Palo Alto.
It lets users generate XML beams without assuming any prior knowledge of the underlying XML-schema rules.
This version is based on the schema for TrueBeam 1.5 and 1.6. For questions, please send us an email at: TrueBeamDeveloper@varian.com

