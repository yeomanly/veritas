Installation
============

Welcome to the installation page.

Requirements
------------

The list of dependencies are as follows:

* `Python 2.7 <http://www.python.org/>`_
* `PySide 1.20 <https://pypi.python.org/pypi/PySide/1.2.0>`_
* `pydicom 0.9.8 <https://pypi.python.org/pypi/pydicom/>`_
* `matplotlib 1.3.1 <http://matplotlib.org/>`_

These librabries can installed either through `easy_install <https://pypi.python.org/pypi/setuptools>`_

.. 
   easy_install SomePackage

or `pip <https://pypi.python.org/pypi/pip>`_

..
   pip install SomePackage


Supported platform
------------------

* Microsoft Windows 7
* GNU/Linux - ubuntu 12.04 LTS

