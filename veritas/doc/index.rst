.. Varian Veritas documentation master file, created by
   sphinx-quickstart on Tue Apr 22 20:22:31 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome
=======

Welcome to Varian Veritas version 1.0, an open source tool to facilitate the creation as well as
modficaiton of an XML beam for TrueBeam Developer Mode. A graphical user interface guides a user through a series
of steps for creating a new XML beam or modifying an exiting XML beam. A user may start from an anonymized DICOM
file or previous XML example and introduce delivery modifications, or begin their experiment from scratch. Veritas is
written using exclusively open source Python code and tools, so expert users may download the source code and extend
functionality for their own purposes.

**Contents**

.. toctree::
   :maxdepth: 3
   
   documentation 
   tutorial
   installation
   downloads
   users

Getting Started
---------------

**Installation**

The list of dependencies are as follows:

* `Python 2.7 <http://www.python.org/>`_
* `PySide 1.20 <https://pypi.python.org/pypi/PySide/1.2.0>`_
* `pydicom 0.9.8 <https://pypi.python.org/pypi/pydicom/>`_
* `matplotlib 1.3.1 <http://matplotlib.org/>`_

These librabries can be installed either through
`easy_install <https://pypi.python.org/pypi/setuptools>`_

:: 

   easy_install SomePackage

or `pip <https://pypi.python.org/pypi/pip>`_

:: 

   pip install SomePackage

**Supported platform**

* Microsoft Windows 7
* GNU/Linux - ubuntu 12.04 LTS

**License**

Varian Veritas is distributed under a BSD open source license. For a complete terms and condition,
please click :doc:`here <license>`


More info
---------

**Need Help?**

Please check the faq, the :doc:`api docs <documentation>`. For general Python related questions
please refer to `stackoverflow <http://stackoverflow.com/>`_.

You can file software bugs, patches and feature requests on the bitbucket `tracker <https://bitbucket.org/varianveritas/veritas/issues/>`_ .

To keep up to date with what's going on in Varian Veritas, see the what's new page or browse the `source code <https://bitbucket.org/varianveritas/veritas/>`_

or

::

   git clone https://bitbucket.org/varianveritas/veritas.git

**Citing Varian Veritas**

Please acknowledge this work by citing `our work <http://amos3.aapm.org/disposition.php?abstractID=23602>`_

**Indices and tables**

* :ref:`Index <genindex>`
* :ref:`Modules <modindex>`
* :ref:`search`

.. warning::
   
   **Developer Mode is intended for non-clinical use only and is NOT cleared for use on humans.**