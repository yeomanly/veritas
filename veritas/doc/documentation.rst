Documentation
=============

The Varian Veritas version 1.0 has following three main components. These components are described as follows.

Main window
+++++++++++

The ``mainWindow``
------------------
.. module:: veritas.mainWindow

.. class:: MainWindow

   **GUI function**

   **Wrapper functions**
   
   The ``MainWindow`` class has four functions corresponding to a set of
   functionalities. These functions are:
    
   - Create a new XML beam :func:`openBeamonHeader`   
   - Modify an existing XML plan :func:`openXMLPlan`
   - Add imaging points to an existing XML beam :func:`openImagingWindow`   
   - Generate an XML from DICOM-RT and modify :func:`dcm2xml`
   
	:members: openBeamonHeader, openXMLPlan, openImagingWindow, dcm2xml
   
   **Utitlity functions**
   
   The ``MainWindow`` has a few utitlity functions to help save and analyze the 
   XML beam has been generated or modified.
   
   :members: exportXML, openTextEditor, displayOutput, openTextEditor
   
   **Miscellaneous functions**

Functionalities
+++++++++++++++

The ``MV beamON`` module
------------------------
.. module:: veritas.beamon
.. autoclass:: beamonHeader
	:members: 

The ``kv and MV imaging`` module
--------------------------------

   A user can insert both kv and MV images for a given XML file. The kv Images has following modes available:

   KV images has following vailable

.. module:: veritas.imaging
.. autoclass:: imageWindow
	:members: 
	
Utilitly Functions
++++++++++++++++++

The ``dicom2xml`` module
------------------------
.. module:: veritas.utils.dicom2xml
.. autoclass:: Dicom2Xml
	:members: 
	
Graphical User Interface
++++++++++++++++++++++++

