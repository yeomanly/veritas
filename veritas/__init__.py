"""
Veritas version 1.0 has MV beamON and imaging functionalities.

Veritas has three main files 

mainWindow.py
=============

This module is the entry point for Veritas. It starts with prompting the
user to read and accept the license terms and conditions before proceeding. 
After user agrees to the license, the main window is presented with a MV beamON a
imaging option.

mainWindow provides users two ways of interactions. A user can either start from scratch
walk through different steps for generating various control points. Once the user is done with 
entering the information he/she can press the done button and the final XML based on the 
XML TrueBeam schema (1.5 and 1.6) is generated.

Key features provided by current version of veritas are as follows:

- open and save a file

- open an exiting XML file

- convert an existing DICOM-RT file to an XMl file


beamON.py
============

This module lets the user either enter a new set of points or import the exiting XMl file and 
modify the existing control points. 

The navigation tab lets user access the controls sequentially as well as randomly.

imaging.py
==========

For a given XML file with control points imaging.py lets the user enter a kV as well as MV points for
capturing images. Both single as well as continuous images can be inserted for a given XMl file.


UI module  
=========

This module contains all the 

images
======

resources file

"""