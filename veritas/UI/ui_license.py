# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\pmishra\Desktop\research\DMode\QTGuiBWH\licenseDlg.ui'
#
# Created: Fri May 23 11:34:47 2014
#      by: pyside-uic 0.2.14 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_licenseDlg(object):
    def setupUi(self, licenseDlg):
        licenseDlg.setObjectName("licenseDlg")
        licenseDlg.resize(810, 676)
        self.textBrowser = QtGui.QTextBrowser(licenseDlg)
        self.textBrowser.setGeometry(QtCore.QRect(110, 110, 621, 471))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(12)
        self.textBrowser.setFont(font)
        self.textBrowser.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.textBrowser.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textBrowser.setObjectName("textBrowser")
        self.label = QtGui.QLabel(licenseDlg)
        self.label.setGeometry(QtCore.QRect(70, 30, 171, 21))
        self.label.setStyleSheet("font: bold 14pt \"Times New Roman\";\n"
"")
        self.label.setObjectName("label")
        self.label_2 = QtGui.QLabel(licenseDlg)
        self.label_2.setGeometry(QtCore.QRect(110, 60, 601, 41))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_2.setAutoFillBackground(False)
        self.label_2.setWordWrap(True)
        self.label_2.setObjectName("label_2")
        self.acceptButton = QtGui.QPushButton(licenseDlg)
        self.acceptButton.setGeometry(QtCore.QRect(540, 630, 75, 30))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(12)
        self.acceptButton.setFont(font)
        self.acceptButton.setObjectName("acceptButton")
        self.cancelButton = QtGui.QPushButton(licenseDlg)
        self.cancelButton.setGeometry(QtCore.QRect(630, 630, 75, 30))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(12)
        self.cancelButton.setFont(font)
        self.cancelButton.setObjectName("cancelButton")

        self.retranslateUi(licenseDlg)
        QtCore.QMetaObject.connectSlotsByName(licenseDlg)

    def retranslateUi(self, licenseDlg):
        licenseDlg.setWindowTitle(QtGui.QApplication.translate("licenseDlg", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser.setHtml(QtGui.QApplication.translate("licenseDlg", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Times New Roman\'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">Copyright (c) 2014, Varian Medical Systems, Inc. (VMS)</span> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">All rights reserved.</span> <span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\"> </span> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">1. </span><span style=\" font-family:\'Times New Roman,serif\'; font-weight:600; color:#000000;\">The Program:</span><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\"> The program to which this License pertains, in source code or binary form, with or without modification, is referred to herein as the “Program”.</span> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">2. </span><span style=\" font-family:\'Times New Roman,serif\'; font-weight:600; color:#000000;\">Grant of License:</span><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\"> The owner of the Program (the “Licensor”) hereby grants you </span><span style=\" font-family:\'Times New Roman,serif\'; color:#000000; background-color:#ffffff;\">a worldwide, royalty-free, non-exclusive, sublicensable license to redistribute or use the Program, either alone or as part of a collective work. </span><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">Redistribution and use of the Program may be in source or binary forms, with or without modification.  This license grant is subject to the following contract terms:  </span> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">a)</span><span style=\" font-size:7pt; color:#000000;\">      </span><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">Redistributions of source code must retain the above copyright notice, this list of contract terms and the following disclaimer(s).</span> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">b)</span><span style=\" font-size:7pt; color:#000000;\">      </span><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">Redistributions in binary form must reproduce the above copyright notice, this list of contract terms and the following disclaimer(s) in the documentation and/or other materials provided with the distribution.</span> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">c)</span><span style=\" font-size:7pt; color:#000000;\">      </span><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">All advertising materials mentioning features or use of this software must display the following acknowledgement:  </span> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">“This product includes software developed by Varian Medical Systems, Inc.”</span> <span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\"> </span> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">d)</span><span style=\" font-size:7pt; color:#000000;\">     </span><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">Neither the name of the Licensor, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.</span> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">e)</span><span style=\" font-size:7pt; color:#000000;\">      </span><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">The Program, information contained therein and any data or results derived therefrom, are not intended for clinical use, and neither the Program nor its use for clinical use or any purpose have been validated by any person.  Accordingly, the Program and any data or information derived from the same shall not be used clinically.  As used in this agreement, “clinically” means activity involving (i) the direct observation of patients; (ii) the diagnosis of disease or other conditions in humans; or (iii) the cure, mitigation, therapy, treatment, treatment planning or prevention of disease in humans.</span> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">f)</span><span style=\" font-size:7pt; color:#000000;\">       </span><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">Any person using the Program agrees to defend, indemnify and hold harmless Licensor and its affiliated companies from and against all liability, loss, damage, or expense including attorney’s and expert’s fees incurred in connection with any claim, suit, action, demand or judgment including injury to or death to any person, and loss of or damage to property, resulting from any act or omission of the user including any clinical use, research, decisions, interpretations, or recommendations made or other activities based on the Program information contained therein, or any data or results derived therefrom.</span> <span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\"> </span> </p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">3. </span><span style=\" font-family:\'Times New Roman,serif\'; font-weight:600; color:#000000;\"> Disclaimer of Warranty:</span><span style=\" font-family:\'Times New Roman,serif\'; color:#000000;\">  THE PROGRAM IS PROVIDED \'\'AS IS\'\' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#000000;\">4. </span><span style=\" font-weight:600; color:#000000;\">Limitation of Liability:</span><span style=\" color:#000000;\"> Under no circumstances and under no legal theory, whether in tort (including negligence), contract, or otherwise, shall the Licensor be liable to anyone for any indirect, special, incidental, or consequential damages of any character arising as a result of this License or the use of the Program including, without limitation, damages for loss of goodwill, loss of use, loss of data, loss of profits, work stoppage, computer failure or malfunction, or any and all other commercial damages or losses. This limitation of liability shall not apply to the extent applicable law prohibits such limitation.</span> </p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"><span style=\" color:#000000;\">5. </span><span style=\" font-weight:600; color:#000000;\">Acceptance of Contract Terms:</span><span style=\" color:#000000;\"> By exercising any of the rights granted to you herein, You indicate Your clear and irrevocable acceptance of this License and all of its terms.  </span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"><span style=\" color:#000000;\">6. </span><span style=\" font-weight:600; color:#000000;\">Termination for Patent Action:</span><span style=\" color:#000000;\"> This License shall terminate automatically and You may no longer exercise any of the rights granted to You by this License as of the date You commence an action, including a cross-claim or counterclaim, against Licensor or any licensee alleging that the Program infringes a patent. </span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"><span style=\" color:#000000; background-color:#ffffff;\">7. </span><span style=\" font-weight:600; color:#000000; background-color:#ffffff;\">Definition of &quot;You&quot; in This License:</span><span style=\" color:#000000; background-color:#ffffff;\"> &quot;You&quot; throughout this License, whether in upper or lower case, means an individual or a legal entity exercising rights under, and complying with all of the terms of, this License. For legal entities, &quot;You&quot; includes any entity that controls, is controlled by, or is under common control with you. For purposes of this definition, &quot;control&quot; means (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity.</span> </p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; background-color:#ffffff;\"><span style=\" color:#000000;\">8. </span><span style=\" font-weight:600; color:#000000; background-color:#ffffff;\">Miscellaneous:</span><span style=\" color:#000000;\"> This License represents the complete agreement concerning the subject matter hereof.  If any court or competent authority finds that any provision of this agreement (or part of any provision) is invalid, illegal or unenforceable, that provision or part-provision shall, to the extent required, be deemed to be deleted and/or reformed as necessary, and the validity and enforceability of the other provisions of this agreement shall not be affected.</span> </p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("licenseDlg", "License Agreement", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("licenseDlg", "Please read the following License Agreement. You must accept the terms of this agreement before continuing with the Varian Veritas", None, QtGui.QApplication.UnicodeUTF8))
        self.acceptButton.setText(QtGui.QApplication.translate("licenseDlg", "Accept", None, QtGui.QApplication.UnicodeUTF8))
        self.cancelButton.setText(QtGui.QApplication.translate("licenseDlg", "Cancel", None, QtGui.QApplication.UnicodeUTF8))

